//
//  AppDelegate.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import XCoordinator
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private lazy var mainWindow = UIWindow()
    
    private let router = AppCoordinator().strongRouter
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        FirebaseApp.configure()
        
        router.setRoot(for: mainWindow)
        
        return true
    }

}
