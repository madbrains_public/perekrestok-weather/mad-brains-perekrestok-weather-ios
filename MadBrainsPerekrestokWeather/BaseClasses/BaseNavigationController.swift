//
//  BaseNavigationController.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override var childForStatusBarStyle: UIViewController? {
        topViewController
    }
    
    override var childForStatusBarHidden: UIViewController? {
        topViewController
    }
    
}
