//
//  APIWrapper+LocationSearch.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension APIWrapper {
    
    func performLocationSearch(text: String) -> Single<LocationSearchResponseData?> {
        guard isReachable else {
            return .error(NSError.APIWrapperError.noConnectionError)
        }
        
        let request = LocationSearchRequest(text: text)
        
        return provider.rx
            .request(.locationSearch(request: request))
            .convertNoConnectionError()
            .mapAsDefaultResponse()
    }
    
}
