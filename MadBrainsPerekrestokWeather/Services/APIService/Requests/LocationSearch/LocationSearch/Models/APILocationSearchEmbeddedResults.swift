//
//  APILocationSearchEmbedded.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APILocationSearchEmbeddedResults: Decodable {
    
    let searchResults: [APILocationSearchResult]
    
    private enum CodingKeys: String, CodingKey {
        case searchResults = "city:search-results"
    }
    
}
