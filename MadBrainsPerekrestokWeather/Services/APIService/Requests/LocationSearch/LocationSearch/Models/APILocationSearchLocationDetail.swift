//
//  APILocationSearchLocationDetail.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APILocationSearchLocationDetail: Decodable {
    
    let coordinates: APILocationSearchCoordinates
    
    private enum CodingKeys: String, CodingKey {
        case coordinates = "latlon"
    }
    
}
