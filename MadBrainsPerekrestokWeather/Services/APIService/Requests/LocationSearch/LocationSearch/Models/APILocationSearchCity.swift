//
//  APILocationSearchCity.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

struct APILocationSearchCity: Decodable {
    
    let id: Int
    let location: APILocationSearchLocationDetail
    
    private enum CodingKeys: String, CodingKey {
        case id = "geoname_id"
        case location
    }
    
}
