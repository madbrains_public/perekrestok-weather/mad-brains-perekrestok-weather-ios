//
//  APILocationSearchCoordinates.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APILocationSearchCoordinates: Decodable {
    
    let latitude: Double
    let longitude: Double
    
}
