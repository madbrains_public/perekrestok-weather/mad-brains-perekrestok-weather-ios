//
//  APIEmbeddedCityItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APILocationSearchEmbeddedCity: Decodable {
    
    let city: APILocationSearchCity
    
    private enum CodingKeys: String, CodingKey {
        case city = "city:item"
    }
    
}
