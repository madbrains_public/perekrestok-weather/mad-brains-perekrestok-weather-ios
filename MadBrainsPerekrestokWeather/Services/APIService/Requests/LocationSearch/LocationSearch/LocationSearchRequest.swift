//
//  LocationSearchRequest.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct LocationSearchRequest {
    
    let text: String
    
}

extension LocationSearchRequest {
    
    var requestParameters: [String: Any] {
        ["search": text, "embed": "city:search-results/city:item"]
    }
    
}
