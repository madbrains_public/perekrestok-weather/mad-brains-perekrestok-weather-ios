//
//  LocationSearchResponseData.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct LocationSearchResponseData: Decodable {
    
    let embeddedSearchResults: APILocationSearchEmbeddedResults
    
    private enum CodingKeys: String, CodingKey {
        case embeddedSearchResults = "_embedded"
    }
    
}
