//
//  APIWrapper+Weather.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension APIWrapper {
    
    func getCurrentWeather(locationIds: [String]) -> Single<GetCurrentWeatherResponseData?> {
        guard isReachable else {
            return .error(NSError.APIWrapperError.noConnectionError)
        }
        
        let request = GetCurrentWeatherRequest(locationIds: locationIds)
        
        return provider.rx
            .request(.getCurrentWeather(request: request))
            .convertNoConnectionError()
            .mapAsDefaultResponse()
    }
    
    func getDetailedWeather(latitude: Double, longitude: Double) -> Single<GetDetailedWeatherResponseData?> {
        guard isReachable else {
            return .error(NSError.APIWrapperError.noConnectionError)
        }
        
        let request = GetDetailedWeatherRequest(latitude: latitude, longitude: longitude)
        
        return provider.rx
            .request(.getDetailedWeather(request: request))
            .convertNoConnectionError()
            .mapAsDefaultResponse()
    }
    
}
