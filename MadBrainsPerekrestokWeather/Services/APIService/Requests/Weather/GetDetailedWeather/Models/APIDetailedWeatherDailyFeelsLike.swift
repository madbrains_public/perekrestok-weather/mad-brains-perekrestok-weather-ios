//
//  APIDetailedWeatherDailyFeelsLike.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APIDetailedWeatherDailyFeelsLike: Decodable {
    
    let day: Double
    let night: Double
    let evening: Double
    let morning: Double
    
    private enum CodingKeys: String, CodingKey {
        case day
        case night
        case evening = "eve"
        case morning = "morn"
    }
    
}
