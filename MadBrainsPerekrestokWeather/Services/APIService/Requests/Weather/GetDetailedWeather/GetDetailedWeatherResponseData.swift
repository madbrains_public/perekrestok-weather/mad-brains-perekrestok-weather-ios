//
//  GetDetailedWeatherResponseData.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct GetDetailedWeatherResponseData: Decodable {
    
    let timezone: String
    let current: APIDetailedWeatherCurrentInfo
    let hourly: [APIDetailedWeatherHourlyInfo]
    let daily: [APIDetailedWeatherDailyInfo]
    
}
