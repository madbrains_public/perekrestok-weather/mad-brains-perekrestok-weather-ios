//
//  GetDetailedWeatherRequest.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct GetDetailedWeatherRequest {
    
    let latitude: Double
    let longitude: Double
    
}

extension GetDetailedWeatherRequest {
    
    var requestParameters: [String: Any] {
        [
            "lat": latitude,
            "lon": longitude,
            "appid": Constants.openWeatherMapAPIKey,
            "units": "metric",
            "lang": "ru",
            "exclude": "minutely,alerts"
        ]
    }
    
}
