//
//  GetCurrentWeatherResponseData.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct GetCurrentWeatherResponseData: Decodable {
    
    let count: Int
    let list: [APIWeatherSummary]
    
    private enum CodingKeys: String, CodingKey {
        case count = "cnt"
        case list
    }
    
}
