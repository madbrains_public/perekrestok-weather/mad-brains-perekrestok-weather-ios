//
//  APIWeatherWind.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APIWeatherWind: Decodable {
    
    let speed: Double
    let deg: Double
    
}
