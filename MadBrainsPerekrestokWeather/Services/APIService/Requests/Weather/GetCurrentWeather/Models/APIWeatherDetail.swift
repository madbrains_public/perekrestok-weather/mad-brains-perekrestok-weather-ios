//
//  APIWeatherDetail.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

struct APIWeatherDetail: Decodable {
    
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    var iconUrl: URL? {
        URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png")
    }
    
}
