//
//  APIWeatherSummary.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APIWeatherSummary: Decodable {
    
    let id: Int
    let name: String
    let coord: APIWeatherCoord
    let weather: [APIWeatherDetail]
    let main: APIWeatherMain
    let wind: APIWeatherWind
    
}
