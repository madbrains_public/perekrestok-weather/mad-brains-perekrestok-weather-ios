//
//  APIWeatherCoord.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct APIWeatherCoord: Decodable {
    
    let lon: Double
    let lat: Double
    
}
