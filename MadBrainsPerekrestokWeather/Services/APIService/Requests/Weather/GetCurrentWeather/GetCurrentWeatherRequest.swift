//
//  GetCurrentWeatherRequest.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

struct GetCurrentWeatherRequest {
    
    let locationIds: [String]
    
}

extension GetCurrentWeatherRequest {
    
    var requestParameters: [String: Any] {
        [
            "id": locationIds.joined(separator: ","),
            "appid": Constants.openWeatherMapAPIKey,
            "units": "metric",
            "lang": "ru"
        ]
    }
    
}
