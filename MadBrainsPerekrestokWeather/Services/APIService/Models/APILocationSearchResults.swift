//
//  APILocationSearchResults.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

struct APILocationSearchResult: Decodable {
    
    let links: APILocationSearchLinks
    let matchingAlternateNames: [APILocationSearchAlternateName]
    let matchingFullName: String
    
    private enum CodingKeys: String, CodingKey {
        case links = "_links"
        case matchingAlternateNames = "matching_alternate_names"
        case matchingFullName = "matching_full_name"
    }
    
    var id: String? {
        links.cityItem.href
            .components(separatedBy: "geonameid:")
            .last?
            .trimmingCharacters(in: NSCharacterSet.decimalDigits.inverted)
    }
    
}
