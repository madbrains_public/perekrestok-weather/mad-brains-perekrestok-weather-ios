//
//  RealmLocationModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation
import RealmSwift

class RealmLocationModel: Object {
    
    @objc dynamic var locationId: String = ""
    @objc dynamic var shortName: String = ""
    @objc dynamic var detailedName: String = ""
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    
    override static func primaryKey() -> String? {
        "locationId"
    }
    
    static let minsk: RealmLocationModel = {
        var model = RealmLocationModel()
        
        model.locationId = "625144"
        model.shortName = "Минск"
        model.detailedName = "Minsk, Belarus"
        model.latitude = 53.9
        model.longitude = 27.56667
        
        return model
    }()
    
    static let moscow: RealmLocationModel = {
        var model = RealmLocationModel()
        
        model.locationId = "524901"
        model.shortName = "Москва"
        model.detailedName = "Moscow, Russia"
        model.latitude = 55.75222
        model.longitude = 37.61556
        
        return model
    }()

}
