//
//  RealmService.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa
import RxRealm

class RealmService {
    
    // swiftlint:disable:next force_try
    private let realm = try! Realm()
    
    func getStoredLocations() -> Single<[RealmLocationModel]> {
        Single.create { [unowned self] single in
            single(.success(self.storedLocations))
            return Disposables.create()
        }
    }
    
    func getLocation(by id: String) -> Single<RealmLocationModel?> {
        let object = realm.object(ofType: RealmLocationModel.self, forPrimaryKey: id)
        
        return Observable.just(object)
            .take(1)
            .asSingle()
    }
    
    func storeLocation(_ model: RealmLocationModel) -> Single<Void> {
        realm.rx.save(object: model)
    }
    
    func deleteLocation(with id: String) -> Single<Void> {
        let predicate = NSPredicate(format: "locationId == %@", id)
        
        return realm.rx.delete(predicate: predicate, ofType: RealmLocationModel.self)
    }
    
    func deleteDatabase() -> Single<Void> {
        realm.rx.deleteDatabase()
    }
    
    private var storedLocations: [RealmLocationModel] {
        let objects = Array(realm.objects(RealmLocationModel.self))
        
        guard objects.isEmpty else {
            return objects
        }
        
        let defaultObjects = [RealmLocationModel.minsk, RealmLocationModel.moscow]
        
        do {
            realm.beginWrite()
            realm.add(defaultObjects, update: .all)
            try realm.commitWrite()
        } catch {
            print(error)
        }
        
        return defaultObjects
    }
    
}
