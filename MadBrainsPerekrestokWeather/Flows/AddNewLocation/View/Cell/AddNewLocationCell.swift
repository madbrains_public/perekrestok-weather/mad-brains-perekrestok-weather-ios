//
//  AddNewLocationCell.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit

class AddNewLocationCell: UITableViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!

    func configure(with model: LocationModel) {
        nameLabel.text = "\(model.shortName) (\(model.detailedName))"
    }
    
}
