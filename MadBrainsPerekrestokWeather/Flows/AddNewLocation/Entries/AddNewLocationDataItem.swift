//
//  AddNewLocationDataItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

enum AddNewLocationDataItem: IdentifiableType, Equatable {
    
    case location(model: LocationModel)
    
}

extension AddNewLocationDataItem {
    
    var identity: String {
        switch self {
        case let .location(model): return model.locationId
        }
    }
    
}
