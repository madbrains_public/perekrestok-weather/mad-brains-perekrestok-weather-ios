//
//  AddNewLocationSectionModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

struct AddNewLocationSectionModel: AnimatableSectionModelType {
    
    let identity: String
    
    var items: [AddNewLocationDataItem]
    
}

extension AddNewLocationSectionModel: SectionModelType {
    
    init(original: AddNewLocationSectionModel, items: [AddNewLocationDataItem]) {
        self = original
        self.items = items
    }
    
}
