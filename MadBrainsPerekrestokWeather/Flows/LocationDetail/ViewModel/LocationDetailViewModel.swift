//
//  HomeViewModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxSwift
import RxCocoa
import XCoordinator

class LocationDetailViewModel: BaseViewModel {
    
    private let apiWrapper: APIWrapper
    private let router: WeakRouter<AppRoute>
    private let locationModel: LocationModel
    
    private let loadedData = BehaviorSubject<GetDetailedWeatherResponseData?>(value: nil)
    
    init(apiWrapper: APIWrapper, router: WeakRouter<AppRoute>, locationModel: LocationModel) {
        self.apiWrapper = apiWrapper
        self.router = router
        self.locationModel = locationModel
    }
    
    func transform(input: Input) -> Output {
        let activityTracker = ActivityTracker()
        let errorTracker = ErrorTracker()
        
        // MARK: Обработка ошибок
        
        errorTracker.asDriver()
            .drive(
                onNext: { [weak self] error in
                    let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                        self?.router.trigger(.pop)
                    }
                    
                    self?.router.trigger(
                        .dialog(
                            title: "Ошибка",
                            message: error.localizedDescription,
                            actions: [okAction],
                            style: .alert
                        )
                    )
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Загрузка детальной информации о погоде
        
        let loadedData = self.loadedData.asDriver(onErrorJustReturn: nil)
        
        apiWrapper
            .getDetailedWeather(latitude: locationModel.latitude, longitude: locationModel.longitude)
            .trackActivity(activityTracker)
            .trackError(errorTracker)
            .asDriver(onErrorJustReturn: nil)
            .ignoreNil()
            .drive(
                onNext: { [weak self] response in
                    self?.loadedData.onNext(response)
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Заголовок
        
        let currentWeather = loadedData.map { data in
            data?.current.weather.first?.description.capitalizingFirstLetter()
        }
        
        // MARK: Модели секций для почасового прогноза
        
        let hourlySectionModels: Driver<[LocationDetailHourlySectionModel]> = loadedData
            .map { response in
                guard let response = response else {
                    return []
                }
                
                var neededElements = response.hourly
                    .sorted { lhs, rhs in
                        lhs.timestamp < rhs.timestamp
                    }
                    .prefix(25)
                
                guard let now = neededElements.popFirst() else {
                    return []
                }
                
                let nowItem = LocationDetailHourlyDataItem.hourlyInfo(
                    item: LocationDetailHourlyCellItem(
                        timestamp: now.timestamp,
                        timezone: response.timezone,
                        isNow: true,
                        iconUrl: now.weather.first?.iconUrl,
                        temperature: now.temp
                    )
                )
                
                let nextItems = neededElements
                    .map {
                        LocationDetailHourlyDataItem.hourlyInfo(
                            item: LocationDetailHourlyCellItem(
                                timestamp: $0.timestamp,
                                timezone: response.timezone,
                                isNow: false,
                                iconUrl: $0.weather.first?.iconUrl,
                                temperature: $0.temp
                            )
                        )
                    }
                
                let model = LocationDetailHourlySectionModel(identity: "Main", items: [nowItem] + nextItems)
                
                return [model]
            }
        
        // MARK: Модели секций для ежедневного прогноза
        
        let mappedDailyInfo = loadedData
            .map { response -> (today: LocationDetailDailyCellItem, nextWeek: [LocationDetailDailySectionModel])? in
                guard let response = response else {
                    return nil
                }
                
                var neededElements = response.daily
                    .sorted { lhs, rhs in
                        lhs.timestamp < rhs.timestamp
                    }
                    .prefix(8)
                
                guard let today = neededElements.popFirst() else {
                    return nil
                }
                
                let todayItem = LocationDetailDailyCellItem(
                    timestamp: today.timestamp,
                    timezone: response.timezone,
                    iconUrl: today.weather.first?.iconUrl,
                    dayTemperature: today.temp.day,
                    nightTemperature: today.temp.night
                )
                
                let nextItems = neededElements
                    .map {
                        LocationDetailDailyDataItem.dailyInfo(
                            item: LocationDetailDailyCellItem(
                                timestamp: $0.timestamp,
                                timezone: response.timezone,
                                iconUrl: $0.weather.first?.iconUrl,
                                dayTemperature: $0.temp.day,
                                nightTemperature: $0.temp.night
                            )
                        )
                    }
                
                let model = LocationDetailDailySectionModel(identity: "Main", items: nextItems)
                
                return (todayItem, [model])
            }
        
        // MARK: Модели секций для отображения подробной текущей погоды
        
        let todaySectionModels = loadedData
            .map { response -> [LocationDetailTodaySectionModel] in
                guard let response = response else {
                    return []
                }
                
                let dateFormatter = DateFormatter()
                
                dateFormatter.timeZone = TimeZone(identifier: response.timezone) ?? .current
                dateFormatter.locale = .init(identifier: "RU")
                dateFormatter.dateFormat = "HH:mm"
                
                let sunriseItem = LocationDetailTodayDataItem.information(
                    key: "ВОСХОД",
                    value: dateFormatter.string(from: Date(timeIntervalSince1970: response.current.sunrise.double)),
                    isLast: false
                )
                
                let sunsetItem = LocationDetailTodayDataItem.information(
                    key: "ЗАКАТ",
                    value: dateFormatter.string(from: Date(timeIntervalSince1970: response.current.sunset.double)),
                    isLast: false
                )
                
                let humidityItem = LocationDetailTodayDataItem.information(
                    key: "ВЛАЖНОСТЬ",
                    value: response.current.humidity.rounded().int.string + " %",
                    isLast: false
                )
                
                let numberFormatter = NumberFormatter()
                numberFormatter.minimumFractionDigits = 0
                numberFormatter.maximumFractionDigits = 2
                numberFormatter.numberStyle = .decimal
                
                let formattedWindSpeed = numberFormatter.string(from: NSNumber(value: response.current.windSpeed))
                let windSpeedString = formattedWindSpeed ?? response.current.windSpeed.string
                
                let windItem = LocationDetailTodayDataItem.information(
                    key: "ВЕТЕР",
                    value: response.current.windDeg.asWindDirection + ", \(windSpeedString) м/с",
                    isLast: false
                )
                
                let feelsLikeItem = LocationDetailTodayDataItem.information(
                    key: "ЧУВСТВУЕТСЯ КАК",
                    value: response.current.feelsLike.asTemperatureString,
                    isLast: false
                )
                
                let pressureInMmhg = response.current.pressure * 0.75006
                let formattedPressure = numberFormatter.string(from: NSNumber(value: pressureInMmhg))
                let pressureString = formattedPressure ?? pressureInMmhg.string
                
                let pressureItem = LocationDetailTodayDataItem.information(
                    key: "ДАВЛЕНИЕ",
                    value: pressureString + " мм рт. ст.",
                    isLast: false
                )
                
                let visibilityInKms = response.current.visibility / 1000
                let formattedVisibility = numberFormatter.string(from: NSNumber(value: visibilityInKms))
                let visibilityString = formattedVisibility ?? visibilityInKms.string
                
                let visibilityItem = LocationDetailTodayDataItem.information(
                    key: "ВИДИМОСТЬ",
                    value: visibilityString + " км",
                    isLast: true
                )
                
                let items = [
                    sunriseItem, sunsetItem, humidityItem, windItem, feelsLikeItem, pressureItem, visibilityItem
                ]
                
                let model = LocationDetailTodaySectionModel(identity: "Main", items: items)
                
                return [model]
            }
        
        let isDataSuccessfullyLoaded = loadedData.map { $0 != nil }
        
        return Output(
            locationName: .just(locationModel.shortName),
            currentWeather: currentWeather,
            currentTemperature: loadedData.map { $0?.current.temp },
            hourlySectionModels: hourlySectionModels,
            mappedDailyInfo: mappedDailyInfo,
            todaySectionModels: todaySectionModels,
            isLoading: activityTracker.asDriver(),
            isDataSuccessfullyLoaded: isDataSuccessfullyLoaded
        )
    }
    
}

extension LocationDetailViewModel {
    
    struct Input {
        
    }
    
    struct Output {
        let locationName: Driver<String>
        let currentWeather: Driver<String?>
        let currentTemperature: Driver<Double?>
        let hourlySectionModels: Driver<[LocationDetailHourlySectionModel]>
        let mappedDailyInfo: Driver<(today: LocationDetailDailyCellItem, nextWeek: [LocationDetailDailySectionModel])?>
        let todaySectionModels: Driver<[LocationDetailTodaySectionModel]>
        let isLoading: Driver<Bool>
        let isDataSuccessfullyLoaded: Driver<Bool>
    }
    
}
