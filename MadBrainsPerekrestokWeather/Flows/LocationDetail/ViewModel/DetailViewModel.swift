//
//  HomeViewModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxSwift
import RxCocoa
import XCoordinator

class LocationDetailViewModel: BaseViewModel {
    
    private let apiWrapper: APIWrapper
    private let router: WeakRouter<AppRoute>
    private let locationModel: LocationModel
    
    init(apiWrapper: APIWrapper, router: WeakRouter<AppRoute>, locationModel: LocationModel) {
        self.apiWrapper = apiWrapper
        self.router = router
        self.locationModel = locationModel
    }
    
    func transform(input: Input) -> Output {
        Output()
    }
    
}

extension LocationDetailViewModel {
    
    struct Input {
        
    }
    
    struct Output {
        
    }
    
}
