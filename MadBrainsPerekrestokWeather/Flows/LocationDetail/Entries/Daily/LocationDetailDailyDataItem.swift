//
//  LocationDetailDailyDataItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

enum LocationDetailDailyDataItem: IdentifiableType, Equatable {
    
    case dailyInfo(item: LocationDetailDailyCellItem)
    
}

extension LocationDetailDailyDataItem {
    
    var identity: String {
        switch self {
        case let .dailyInfo(item): return item.timestamp.string
        }
    }
    
}
