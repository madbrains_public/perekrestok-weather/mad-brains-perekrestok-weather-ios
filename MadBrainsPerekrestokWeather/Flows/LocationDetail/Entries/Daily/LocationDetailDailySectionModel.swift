//
//  LocationDetailDailySectionModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

struct LocationDetailDailySectionModel: AnimatableSectionModelType {
    
    let identity: String
    
    var items: [LocationDetailDailyDataItem]
    
}

extension LocationDetailDailySectionModel: SectionModelType {
    
    init(original: LocationDetailDailySectionModel, items: [LocationDetailDailyDataItem]) {
        self = original
        self.items = items
    }
    
}
