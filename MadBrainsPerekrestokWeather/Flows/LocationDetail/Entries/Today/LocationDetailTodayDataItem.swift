//
//  LocationDetailTodayDataItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

enum LocationDetailTodayDataItem: IdentifiableType, Equatable {
    
    case information(key: String, value: String, isLast: Bool)
    
}

extension LocationDetailTodayDataItem {
    
    var identity: String {
        switch self {
        case let .information(key, _, _): return key
        }
    }
    
}
