//
//  LocationDetailTodaySectionModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

struct LocationDetailTodaySectionModel: AnimatableSectionModelType {
    
    let identity: String
    
    var items: [LocationDetailTodayDataItem]
    
}

extension LocationDetailTodaySectionModel: SectionModelType {
    
    init(original: LocationDetailTodaySectionModel, items: [LocationDetailTodayDataItem]) {
        self = original
        self.items = items
    }
    
}
