//
//  LocationDetailHourlyDataItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

enum LocationDetailHourlyDataItem: IdentifiableType, Equatable {
    
    case hourlyInfo(item: LocationDetailHourlyCellItem)
    
}

extension LocationDetailHourlyDataItem {
    
    var identity: String {
        switch self {
        case let .hourlyInfo(item): return item.timestamp.string
        }
    }
    
}
