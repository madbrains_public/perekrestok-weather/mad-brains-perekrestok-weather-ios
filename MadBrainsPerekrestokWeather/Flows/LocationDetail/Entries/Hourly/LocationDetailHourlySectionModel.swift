//
//  LocationDetailHourlySectionModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

struct LocationDetailHourlySectionModel: AnimatableSectionModelType {
    
    let identity: String
    
    var items: [LocationDetailHourlyDataItem]
    
}

extension LocationDetailHourlySectionModel: SectionModelType {
    
    init(original: LocationDetailHourlySectionModel, items: [LocationDetailHourlyDataItem]) {
        self = original
        self.items = items
    }
    
}
