//
//  LocationDetailDailyCellItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

struct LocationDetailDailyCellItem: Equatable {
    
    let timestamp: Int
    let timezone: String
    let iconUrl: URL?
    let dayTemperature: Double
    let nightTemperature: Double
    
}
