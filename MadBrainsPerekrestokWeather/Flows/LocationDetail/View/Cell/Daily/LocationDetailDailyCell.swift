//
//  LocationDetailDailyCell.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import Kingfisher

class LocationDetailDailyCell: UICollectionViewCell {
    
    @IBOutlet private weak var dayNameLabel: UILabel!
    @IBOutlet private weak var weatherIconView: UIImageView!
    @IBOutlet private weak var dayTemperatureLabel: UILabel!
    @IBOutlet private weak var nightTemperatureLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        weatherIconView.kf.cancelDownloadTask()
    }
    
    func configure(item: LocationDetailDailyCellItem) {
        dayNameLabel.text = type(of: self).formattedTime(timestamp: item.timestamp, timezone: item.timezone)
        
        weatherIconView.kf.setImage(with: item.iconUrl)
        
        dayTemperatureLabel.text = item.dayTemperature.asTemperatureString
        nightTemperatureLabel.text = item.nightTemperature.asTemperatureString
    }

}

extension LocationDetailDailyCell {
    
    static func formattedTime(timestamp: Int, timezone: String) -> String {
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone(identifier: timezone) ?? .current
        formatter.locale = .init(identifier: "RU")
        formatter.dateFormat = "EEEE"
        
        return formatter.string(from: Date(timeIntervalSince1970: timestamp.double)).capitalizingFirstLetter()
    }
    
}
