//
//  LocationDetailHourlyCellItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

struct LocationDetailHourlyCellItem: Equatable {
    
    let timestamp: Int
    let timezone: String
    let isNow: Bool
    let iconUrl: URL?
    let temperature: Double
    
}
