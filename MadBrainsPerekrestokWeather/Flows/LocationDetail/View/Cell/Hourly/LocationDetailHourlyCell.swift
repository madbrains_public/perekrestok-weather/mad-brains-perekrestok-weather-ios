//
//  LocationDetailHourlyCell.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import Kingfisher

class LocationDetailHourlyCell: UICollectionViewCell {
    
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var weatherIconView: UIImageView!
    @IBOutlet private weak var temperatureLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        weatherIconView.kf.cancelDownloadTask()
    }
    
    func configure(item: LocationDetailHourlyCellItem) {
        timeLabel.font = UIFont.systemFont(ofSize: 17, weight: item.isNow ? .medium : .regular)
        
        timeLabel.text = item.isNow ?
            "Сейчас" :
            type(of: self).formattedTime(timestamp: item.timestamp, timezone: item.timezone)
        
        weatherIconView.kf.setImage(with: item.iconUrl)
        
        temperatureLabel.font = UIFont.systemFont(ofSize: 17, weight: item.isNow ? .medium : .regular)
        temperatureLabel.text = item.temperature.asTemperatureString
    }

}

extension LocationDetailHourlyCell {
    
    static func formattedTime(timestamp: Int, timezone: String) -> String {
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone(identifier: timezone) ?? .current
        formatter.locale = .init(identifier: "RU")
        formatter.dateFormat = "HH"
        
        return formatter.string(from: Date(timeIntervalSince1970: timestamp.double))
    }
    
    static func preferredWidth(item: LocationDetailHourlyCellItem) -> CGFloat {
        let timeLabel = UILabel()
        timeLabel.font = UIFont.systemFont(ofSize: 17, weight: item.isNow ? .medium : .regular)
        timeLabel.text = item.isNow ? "Сейчас" : formattedTime(timestamp: item.timestamp, timezone: item.timezone)
        
        let preferredTimeLabelSize = timeLabel.sizeThatFits(
            CGSize(width: CGFloat.greatestFiniteMagnitude, height: 20)
        )
        
        let temperatureLabel = UILabel()
        temperatureLabel.font = UIFont.systemFont(ofSize: 17, weight: item.isNow ? .medium : .regular)
        temperatureLabel.text = item.temperature.asTemperatureString
        
        let preferredTemperatureLabelSize = temperatureLabel.sizeThatFits(
            CGSize(width: CGFloat.greatestFiniteMagnitude, height: 20)
        )
        
        return max(preferredTimeLabelSize.width, preferredTemperatureLabelSize.width, 30)
    }
    
}
