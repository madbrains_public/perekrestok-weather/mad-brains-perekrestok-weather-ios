//
//  LocationDetailTodayCell.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit

class LocationDetailTodayCell: UICollectionViewCell {
    
    @IBOutlet private weak var keyLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!
    
    @IBOutlet private var separatorViewHeight: NSLayoutConstraint!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        separatorViewHeight.constant = 1.0 / UIScreen.main.scale
    }
    
    func configure(key: String, value: String, isLast: Bool) {
        keyLabel.text = key
        valueLabel.text = value
        separatorView.isHidden = isLast
    }

}
