//
//  HomeViewController.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import JGProgressHUD

class LocationDetailViewController: BaseViewController, LoaderPresentable {
    
    @IBOutlet private weak var hidingView: UIView!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    // MARK: Header View
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var currentWeatherLabel: UILabel!
    @IBOutlet private weak var currentTemperatureLabel: UILabel!
    
    // MARK: Today Daily View
    
    @IBOutlet private weak var todayDayNameLabel: UILabel!
    @IBOutlet private weak var todayDayTemperatureLabel: UILabel!
    @IBOutlet private weak var todayNightTemperatureLabel: UILabel!
    
    // MARK: Hourly Collection View
    
    @IBOutlet private weak var hourlyCollectionView: UICollectionView!
    
    @IBOutlet private var hourlyTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet private var hourlyBottomSeparatorHeight: NSLayoutConstraint!
    
    private var hourlyDataSource: RxCollectionViewSectionedAnimatedDataSource<LocationDetailHourlySectionModel>?
    
    // MARK: Daily Collection View
    
    @IBOutlet private weak var dailyCollectionView: UICollectionView!
    
    @IBOutlet private var dailyCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet private var dailyBottomSeparatorHeight: NSLayoutConstraint!
    
    private var dailyDataSource: RxCollectionViewSectionedAnimatedDataSource<LocationDetailDailySectionModel>?
    
    // MARK: Today Collection View
    
    @IBOutlet private weak var todayCollectionView: UICollectionView!
    
    @IBOutlet private var todayCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet private var todayBottomSeparatorHeight: NSLayoutConstraint!
    
    private var todayDataSource: RxCollectionViewSectionedAnimatedDataSource<LocationDetailTodaySectionModel>?
    
    // MARK: LoaderPresentable
    
    var loader: JGProgressHUD?
    
    var loaderContainer: UIView {
        navigationController?.view ?? view
    }
    
    private let viewModel: LocationDetailViewModel
    
    init(viewModel: LocationDetailViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        bind()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        hourlyTopSeparatorHeight.constant = 1.0 / UIScreen.main.scale
        hourlyBottomSeparatorHeight.constant = 1.0 / UIScreen.main.scale
        dailyBottomSeparatorHeight.constant = 1.0 / UIScreen.main.scale
        todayBottomSeparatorHeight.constant = 1.0 / UIScreen.main.scale
    }
    
    private func configureView() {
        title = "Погода"
        
        configureHourlyCollectionView()
        configureDailyCollectionView()
        configureTodayCollectionView()
    }
    
    private func configureHourlyCollectionView() {
        hourlyCollectionView.register(nibWithCellClass: LocationDetailHourlyCell.self)
        
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<LocationDetailHourlySectionModel>(
            configureCell: { _, collectionView, indexPath, item in
                switch item {
                case let .hourlyInfo(innerItem):
                    let cell = collectionView.dequeueReusableCell(
                        withClass: LocationDetailHourlyCell.self,
                        for: indexPath
                    )
                    
                    cell.configure(item: innerItem)
                    
                    return cell
                }
            }
        )
        
        hourlyCollectionView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.hourlyDataSource = dataSource
    }
    
    private func configureDailyCollectionView() {
        dailyCollectionView.register(nibWithCellClass: LocationDetailDailyCell.self)
        
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<LocationDetailDailySectionModel>(
            configureCell: { _, collectionView, indexPath, item in
                switch item {
                case let .dailyInfo(innerItem):
                    let cell = collectionView.dequeueReusableCell(
                        withClass: LocationDetailDailyCell.self,
                        for: indexPath
                    )
                    
                    cell.configure(item: innerItem)
                    
                    return cell
                }
            }
        )
        
        dailyCollectionView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.dailyDataSource = dataSource
    }
    
    private func configureTodayCollectionView() {
        todayCollectionView.register(nibWithCellClass: LocationDetailTodayCell.self)

        let dataSource = RxCollectionViewSectionedAnimatedDataSource<LocationDetailTodaySectionModel>(
            configureCell: { _, collectionView, indexPath, item in
                switch item {
                case let .information(key, value, isLast):
                    let cell = collectionView.dequeueReusableCell(
                        withClass: LocationDetailTodayCell.self,
                        for: indexPath
                    )

                    cell.configure(key: key, value: value, isLast: isLast)

                    return cell
                }
            }
        )

        todayCollectionView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)

        self.todayDataSource = dataSource
    }
    
    private func applyTodayDailyItem(_ todayItem: LocationDetailDailyCellItem?) {
        todayDayNameLabel.text = "Сегодня"
        todayDayTemperatureLabel.text = todayItem?.dayTemperature.asTemperatureString
        todayNightTemperatureLabel.text = todayItem?.nightTemperature.asTemperatureString
    }
    
    private func bind() {
        dailyCollectionView.rx
            .observeWeakly(CGSize.self, "contentSize")
            .subscribe(
                onNext: { [weak self] size in
                    guard let self = self, let uSize = size else {
                        return
                    }
                    
                    self.dailyCollectionViewHeight.constant = uSize.height
                    self.view.setNeedsLayout()
                }
            )
            .disposed(by: disposeBag)
        
        todayCollectionView.rx
            .observeWeakly(CGSize.self, "contentSize")
            .subscribe(
                onNext: { [weak self] size in
                    guard let self = self, let uSize = size else {
                        return
                    }
                    
                    self.todayCollectionViewHeight.constant = uSize.height
                    self.view.setNeedsLayout()
                }
            )
            .disposed(by: disposeBag)
        
        let input = LocationDetailViewModel.Input()
        let output = viewModel.transform(input: input)
        
        output.locationName
            .drive(titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        output.currentWeather
            .drive(currentWeatherLabel.rx.text)
            .disposed(by: disposeBag)
        
        output.currentTemperature
            .map { temperature -> String? in
                guard let temperature = temperature else {
                    return nil
                }
                
                return " " + temperature.asTemperatureString
            }
            .drive(currentTemperatureLabel.rx.text)
            .disposed(by: disposeBag)
        
        output.hourlySectionModels
            .drive(hourlyCollectionView.rx.items(dataSource: hourlyDataSource!))
            .disposed(by: disposeBag)
        
        output.mappedDailyInfo
            .map { $0?.today }
            .drive(
                onNext: { [weak self] todayItem in
                    self?.applyTodayDailyItem(todayItem)
                }
            )
            .disposed(by: disposeBag)
        
        output.mappedDailyInfo
            .map { $0?.nextWeek ?? [] }
            .drive(dailyCollectionView.rx.items(dataSource: dailyDataSource!))
            .disposed(by: disposeBag)
        
        output.todaySectionModels
            .drive(todayCollectionView.rx.items(dataSource: todayDataSource!))
            .disposed(by: disposeBag)
        
        output.isLoading
            .debounce(.milliseconds(25))
            .drive(
                onNext: { [weak self] isLoading in
                    self?.updateLoader(isEnabled: isLoading, detailText: nil)
                }
            )
            .disposed(by: disposeBag)
        
        output.isDataSuccessfullyLoaded
            .isTrue()
            .delay(.milliseconds(500))
            .drive(hidingView.rx.isHidden)
            .disposed(by: disposeBag)
        
        output.isDataSuccessfullyLoaded
            .filterFalse()
            .drive(hidingView.rx.isHidden)
            .disposed(by: disposeBag)
    }

}

extension LocationDetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        switch collectionView {
        case hourlyCollectionView:
            guard let item = hourlyDataSource?[indexPath],
                case let .hourlyInfo(innerItem) = item else {
                return .zero
            }
            
            let preferredWidth = LocationDetailHourlyCell.preferredWidth(item: innerItem)
            
            return CGSize(width: preferredWidth, height: 100)
            
        case dailyCollectionView:
            return CGSize(width: collectionView.bounds.width, height: 40)
            
        case todayCollectionView:
            return CGSize(width: collectionView.bounds.width, height: 60)
            
        default:
            return .zero
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        switch collectionView {
        case hourlyCollectionView: return 10
        default: return 0
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        switch collectionView {
        case hourlyCollectionView: return 10
        default: return 0
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        switch collectionView {
        case hourlyCollectionView: return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        default: return .zero
        }
    }
    
}
