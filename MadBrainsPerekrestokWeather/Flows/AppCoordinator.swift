//
//  AppCoordinator.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import XCoordinator
import RxSwift
import RxCocoa
import Realm
import RealmSwift

enum AppRoute: Route {
    
    case locationsList
    case addNewLocation(delegate: AddNewLocationViewModelDelegate)
    case locationDetail(locationModel: LocationModel)
    
    case alert(title: String?, message: String?)
    case dialog(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style)
    
    case dismiss
    case pop
    
}

class AppCoordinator: NavigationCoordinator<AppRoute> {
    
    private let disposeBag = DisposeBag()
    
    private let apiWrapper = APIWrapper()
    private let realmService = RealmService()
    
    init() {
        super.init(rootViewController: BaseNavigationController(), initialRoute: .locationsList)
        
        rootViewController.setNavigationBarHidden(true, animated: false)
    }
    
    override func prepareTransition(for route: AppRoute) -> NavigationTransition {
        switch route {
        case .locationsList:
            let vm = LocationsListViewModel(apiWrapper: apiWrapper, realmService: realmService, router: weakRouter)
            let vc = LocationsListViewController(viewModel: vm)
            return .set([vc])
            
        case let .addNewLocation(delegate):
            let vm = AddNewLocationViewModel(
                apiWrapper: apiWrapper,
                realmService: realmService,
                delegate: delegate,
                router: weakRouter
            )
            
            let vc = AddNewLocationViewController(viewModel: vm)
            return .present(vc)
            
        case let .locationDetail(locationModel):
            let vm = LocationDetailViewModel(apiWrapper: apiWrapper, router: weakRouter, locationModel: locationModel)
            let vc = LocationDetailViewController(viewModel: vm)
            return .push(vc)
            
        case let .alert(title, message):
            return .alertTransition(title: title, message: message)
            
        case let .dialog(title, message, actions, style):
            return .dialogTransition(title: title, message: message, actions: actions, style: style)
            
        case .dismiss:
            return .dismiss()
            
        case .pop:
            return .pop()
        }
    }
    
}
