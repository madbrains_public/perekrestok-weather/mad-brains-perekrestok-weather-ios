//
//  LocationsListDataItem.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

enum LocationsListDataItem: IdentifiableType, Equatable {
    
    case preview(location: LocationModel, temperature: Double, iconUrl: URL?, weatherDescription: String?)
    
}

extension LocationsListDataItem {
    
    var identity: String {
        switch self {
        case let .preview(location, _, _, _): return location.locationId
        }
    }
    
}
