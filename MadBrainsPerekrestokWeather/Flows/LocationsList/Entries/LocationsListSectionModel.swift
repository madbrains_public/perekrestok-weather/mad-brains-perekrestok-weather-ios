//
//  LocationsListSectionModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxDataSources

struct LocationsListSectionModel: AnimatableSectionModelType {
    
    let identity: String
    
    var items: [LocationsListDataItem]
    
}

extension LocationsListSectionModel: SectionModelType {
    
    init(original: LocationsListSectionModel, items: [LocationsListDataItem]) {
        self = original
        self.items = items
    }
    
}
