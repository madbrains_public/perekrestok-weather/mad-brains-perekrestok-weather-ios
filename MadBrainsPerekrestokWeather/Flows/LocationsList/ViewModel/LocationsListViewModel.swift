//
//  LocationsListViewModel.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import RxSwift
import RxCocoa
import XCoordinator

class LocationsListViewModel: BaseViewModel {
    
    private let apiWrapper: APIWrapper
    private let realmService: RealmService
    private let router: WeakRouter<AppRoute>
    
    private let loadedLocations = BehaviorSubject<[LocationModel]>(value: [])
    private let loadedWeather = BehaviorSubject<[APIWeatherSummary]>(value: [])
    
    init(apiWrapper: APIWrapper, realmService: RealmService, router: WeakRouter<AppRoute>) {
        self.apiWrapper = apiWrapper
        self.realmService = realmService
        self.router = router
    }
    
    func transform(input: Input) -> Output {
        let activityTracker = ActivityTracker()
        let errorTracker = ErrorTracker()
        
        // MARK: Обработка ошибок
        
        let retryAfterErrorTrigger = PublishSubject<Void>()
        
        errorTracker.asDriver()
            .drive(
                onNext: { [weak self] error in
                    let retryAction = UIAlertAction(title: "Попробовать снова", style: .default) { _ in
                        retryAfterErrorTrigger.onNext(())
                    }
                    
                    self?.router.trigger(
                        .dialog(
                            title: "Ошибка",
                            message: error.localizedDescription,
                            actions: [retryAction],
                            style: .alert
                        )
                    )
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Модели для отображения ячеек с погодой в списке
        
        let loadedLocations = self.loadedLocations.asDriver(onErrorJustReturn: [])
        let loadedWeather = self.loadedWeather.asDriver(onErrorJustReturn: [])
        
        let sectionModels = Driver
            .combineLatest(loadedLocations, loadedWeather)
            .map { locations, weather -> [LocationsListSectionModel] in
                let items = locations.compactMap { locationModel -> LocationsListDataItem? in
                    guard let relatedWeather = (weather.first {
                        $0.id.string == locationModel.locationId
                    }) else {
                        return nil
                    }
                    
                    return LocationsListDataItem.preview(
                        location: locationModel,
                        temperature: relatedWeather.main.temp,
                        iconUrl: relatedWeather.weather.first?.iconUrl,
                        weatherDescription: relatedWeather.weather.first?.description
                    )
                }
                
                let sectionModel = LocationsListSectionModel(identity: "Main", items: items)
                
                return [sectionModel]
            }
        
        // MARK: Триггер для полной перезагрузки данных
        
        let refreshDataTrigger = Driver.merge(
            retryAfterErrorTrigger.asDriverOnErrorJustComplete(),
            .just(())
        )

        refreshDataTrigger
            .flatMap { [weak self] _ -> Driver<[RealmLocationModel]> in
                guard let self = self else {
                    return .empty()
                }

                return self.realmService
                    .getStoredLocations()
                    .trackActivity(activityTracker)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .map { realmModels in
                realmModels.map { LocationModel(realmModel: $0) }
            }
            .do(
                onNext: { [weak self] locationModels in
                    self?.loadedLocations.onNext(locationModels)
                }
            )
            .map { locationModels -> [[String]] in
                locationModels.map { $0.locationId }.group(by: 15) ?? []
            }
            .delay(.milliseconds(50))
            .flatMap { [weak self] groupedIds -> Driver<[APIWeatherSummary]> in
                guard let self = self else {
                    return .empty()
                }
                
                let queries = groupedIds.map { self.apiWrapper.getCurrentWeather(locationIds: $0) }
                
                return Single
                    .zip(queries)
                    .trackActivity(activityTracker)
                    .trackError(errorTracker)
                    .map { responses in
                        responses.compactMap { $0 }.map { $0.list }.flatMap { $0 }
                    }
                    .asDriverOnErrorJustComplete()
            }
            .drive(
                onNext: { [weak self] loadedWeather in
                    self?.loadedWeather.onNext(loadedWeather)
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Удаление локации из списка
        
        input.deleteLocationTrigger
            .flatMap { [weak self] id -> Driver<Void?> in
                guard let self = self else {
                    return .empty()
                }
                
                return self.realmService
                    .deleteLocation(with: id)
                    .mapToOptional()
                    .trackActivity(activityTracker)
                    .trackError(errorTracker)
                    .asDriver(onErrorJustReturn: nil)
            }
            .ignoreNil()
            .flatMap { [weak self] _ -> Driver<[RealmLocationModel]> in
                guard let self = self else {
                    return .empty()
                }

                return self.realmService
                    .getStoredLocations()
                    .trackActivity(activityTracker)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .map { realmModels in
                realmModels.map { LocationModel(realmModel: $0) }
            }
            .drive(
                onNext: { [weak self] actualModels in
                    self?.loadedLocations.onNext(actualModels)
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Триггер нажатия на кнопку "Добавить локацию"
        
        input.addLocationTrigger
            .drive(
                onNext: { [weak self] in
                    guard let self = self else {
                        return
                    }
                    
                    self.router.trigger(.addNewLocation(delegate: self))
                }
            )
            .disposed(by: disposeBag)
        
        // MARK: Триггер нажатия на ячейку
        
        input.itemSelected
            .withLatestFrom(sectionModels) { ($0, $1) }
            .map { indexPath, sectionModels -> LocationModel? in
                guard let item = sectionModels[safe: indexPath.section]?.items[safe: indexPath.row],
                    case let .preview(location, _, _, _) = item else {
                    return nil
                }
                
                return location
            }
            .ignoreNil()
            .drive(
                onNext: { [weak self] locationModel in
                    self?.router.trigger(.locationDetail(locationModel: locationModel))
                }
            )
            .disposed(by: disposeBag)
        
        return Output(
            sectionModels: sectionModels,
            isLoading: activityTracker.asDriver()
        )
    }
    
}

extension LocationsListViewModel {
    
    struct Input {
        let addLocationTrigger: Driver<Void>
        let deleteLocationTrigger: Driver<String>
        let itemSelected: Driver<IndexPath>
    }
    
    struct Output {
        let sectionModels: Driver<[LocationsListSectionModel]>
        let isLoading: Driver<Bool>
    }
    
}

extension LocationsListViewModel: AddNewLocationViewModelDelegate {
    
    func addNewLocationViewModel(
        _ viewModel: AddNewLocationViewModel,
        didSuccessfullyAddNewLocation location: LocationModel,
        withCurrentWeather weather: APIWeatherSummary
    ) {
        realmService
            .getStoredLocations()
            .asDriverOnErrorJustComplete()
            .map { realmModels in
                realmModels.map { LocationModel(realmModel: $0) }
            }
            .do(
                onNext: { [weak self] actualLocationModels in
                    self?.loadedLocations.onNext(actualLocationModels)
                }
            )
            .withLatestFrom(loadedWeather.asDriver(onErrorJustReturn: []))
            .map { currentWeatherData in
                var mutableData = currentWeatherData
                
                mutableData.removeAll { $0.id == weather.id }
                mutableData.append(weather)
                
                return mutableData
            }
            .drive(
                onNext: { [weak self] actualWeatherData in
                    self?.loadedWeather.onNext(actualWeatherData)
                }
            )
            .disposed(by: disposeBag)
    }
    
}
