//
//  LocationsListCell.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import Kingfisher

class LocationsListCell: UITableViewCell {
    
    @IBOutlet private weak var locationNameLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var weatherIconView: UIImageView!
    @IBOutlet private weak var weatherDescriptionLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        weatherIconView.kf.cancelDownloadTask()
    }
    
    func configure(
        location: LocationModel,
        temperature: Double,
        iconUrl: URL?,
        weatherDescription: String?
    ) {
        locationNameLabel.text = "\(location.shortName) (\(location.detailedName))"
        temperatureLabel.text = temperature.asTemperatureString
        
        weatherIconView.kf.setImage(with: iconUrl)
        
        weatherDescriptionLabel.isHidden = weatherDescription.isNilOrEmpty
        weatherDescriptionLabel.text = weatherDescription?.capitalizingFirstLetter()
    }
    
}
