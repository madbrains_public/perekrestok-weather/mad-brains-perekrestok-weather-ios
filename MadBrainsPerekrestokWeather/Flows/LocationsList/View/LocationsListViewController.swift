//
//  LocationsListViewController.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 22.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import JGProgressHUD

class LocationsListViewController: BaseViewController, LoaderPresentable {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var dataSource: RxTableViewSectionedAnimatedDataSource<LocationsListSectionModel>?
    
    var loader: JGProgressHUD?
    
    var loaderContainer: UIView {
        navigationController?.view ?? view
    }

    private let addButton = UIBarButtonItem(
        barButtonSystemItem: .add,
        target: nil,
        action: nil
    )
    
    private let viewModel: LocationsListViewModel
    private let deleteLocationTrigger = PublishSubject<String>()
    
    init(viewModel: LocationsListViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        bind()
    }
    
    private func configureView() {
        title = "Ваши локации"
        
        navigationItem.setRightBarButton(addButton, animated: false)
        
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.register(nibWithCellClass: LocationsListCell.self)
        
        let dataSource = RxTableViewSectionedAnimatedDataSource<LocationsListSectionModel>(
            configureCell: { _, tableView, indexPath, item in
                switch item {
                case let .preview(location, temperature, iconUrl, weatherDescription):
                    let cell = tableView.dequeueReusableCell(withClass: LocationsListCell.self, for: indexPath)
                    
                    cell.separatorInset = .zero
                    
                    cell.configure(
                        location: location,
                        temperature: temperature,
                        iconUrl: iconUrl,
                        weatherDescription: weatherDescription
                    )
                    
                    return cell
                }
            }
        )
        
        dataSource.canEditRowAtIndexPath = { dataSource, indexPath in
            let ignoredIds = [RealmLocationModel.minsk.locationId, RealmLocationModel.moscow.locationId]
            return !ignoredIds.contains(dataSource[indexPath].identity)
        }
        
        dataSource.animationConfiguration = .init(
            insertAnimation: .fade,
            reloadAnimation: .fade,
            deleteAnimation: .fade
        )
        
        tableView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        self.dataSource = dataSource
    }
    
    private func bind() {
        let input = LocationsListViewModel.Input(
            addLocationTrigger: addButton.rx.tap.asDriver(),
            deleteLocationTrigger: deleteLocationTrigger.asDriverOnErrorJustComplete(),
            itemSelected: tableView.rx.itemSelected.asDriver()
        )
        
        let output = viewModel.transform(input: input)
        
        output.sectionModels
            .drive(tableView.rx.items(dataSource: dataSource!))
            .disposed(by: disposeBag)
        
        output.isLoading
            .debounce(.milliseconds(25))
            .drive(
                onNext: { [weak self] isLoading in
                    self?.updateLoader(isEnabled: isLoading, detailText: nil)
                }
            )
            .disposed(by: disposeBag)
    }

}

extension LocationsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(
            style: .default,
            title: "Удалить"
        ) { [weak self] _, indexPath in
            guard let item = self?.dataSource?[indexPath], case let .preview(location, _, _, _) = item else {
                return
            }
            
            self?.deleteLocationTrigger.onNext(location.locationId)
        }
        
        deleteButton.title = "Удалить"
        
        return [deleteButton]
    }
    
}
