//
//  NSError+APIWrapper.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 21.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

extension NSError {
    
    enum APIWrapperError {
        
        static let domain = "APIWrapperError"
        static let responseStatusCodeKey = "responseStatusCodeKey"
        
        /// Запрос зафейлился. Известен только код ошибки
        static func codeIsNotSuccessful(_ code: Int) -> NSError {
            NSError(
                domain: domain,
                code: 1000,
                userInfo: [
                    responseStatusCodeKey: code,
                    NSLocalizedDescriptionKey: "В ходе выполнения запроса произошла ошибка \(code)"
                ]
            )
        }
        
        /// Ошибка отсутствия соединения
        static let noConnectionError = NSError(
            domain: domain,
            code: 1001,
            userInfo: [NSLocalizedDescriptionKey: "Нет соединения"]
        )
        
        /// Ошибка маппинга запроса с успешным кодом
        static let successfulResponseMappingError = NSError(
            domain: domain,
            code: 1002,
            userInfo: [NSLocalizedDescriptionKey: "Ошибка маппинга респонза в запросе с успешным кодом"]
        )
        
    }
    
}
