//
//  Transition+Basic.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 27.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import XCoordinator

extension Transition {
    
    static func alertTransition(title: String?, message: String?) -> Transition {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        return .present(alert)
    }
    
    static func dialogTransition(
        title: String?,
        message: String?,
        actions: [UIAlertAction],
        style: UIAlertController.Style
    ) -> Transition {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        
        actions.forEach {
            alert.addAction($0)
        }
        
        return .present(alert)
    }
    
}
