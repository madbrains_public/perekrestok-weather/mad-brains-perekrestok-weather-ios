//
//  Double+WindDirections.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 26.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

extension Double {
    
    var asWindDirection: String {
        let directions = [
            "С", "ССВ", "СВ", "ВСВ", "В", "ВЮВ", "ЮВ", "ЮЮВ", "Ю", "ЮЮЗ", "ЮЗ", "ЗЮЗ", "З", "ЗСЗ", "СЗ", "ССЗ"
        ]
        
        let position = Int((self + 11.25) / 22.5)
        return directions[position % 16]
    }
    
}
