//
//  NSError+RealmService.swift
//  MadBrainsPerekrestokWeather
//
//  Created by Alexander Khiger on 23.10.2020.
//  Copyright © 2020 Mad Brains. All rights reserved.
//

import Foundation

extension NSError {
    
    enum RealmServiceError {
        
        private static let domain = "RealmServiceError"
        
        /// CurrentlyInWriteTransaction
        static let currentlyInWriteTransaction = NSError(
            domain: domain,
            code: 2000,
            userInfo: [NSLocalizedDescriptionKey: "Realm уже находится в процессе транзакции записи данных"]
        )
        
    }
    
}
