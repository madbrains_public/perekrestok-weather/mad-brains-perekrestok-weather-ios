platform :ios, '12.0'
inhibit_all_warnings!

source 'https://github.com/cocoapods/specs.git'

target 'MadBrainsPerekrestokWeather' do

  use_frameworks!

  # Networking
  pod 'Moya/RxSwift'
  pod 'Kingfisher'

  # Reactive
  pod 'RxSwift'
  pod 'RxCocoa'
  pod 'RxViewController'
  pod 'RxDataSources'

  # Utilities
  pod 'SwiftLint'
  pod 'SwifterSwift', '4.6'
  
  # UI
  pod 'JGProgressHUD'
  
  # Realm
  pod 'Realm'
  pod 'RealmSwift'
  pod 'RxRealm'
  
  # Routing
  pod 'XCoordinator'
  pod 'XCoordinator/RxSwift'
  
  # Firebase
  pod 'Firebase/Analytics'
  
end

post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.0'
            
            if config.name == 'Release'
                config.build_settings['SWIFT_COMPILATION_MODE'] = 'wholemodule'
            end
        end
    end
end
